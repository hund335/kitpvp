package mc.hund35.kitpvp;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import mc.hund35.kitpvp.admincmds.setspawn;
import mc.hund35.kitpvp.cmds.kit;
import mc.hund35.kitpvp.cmds.msg;
import mc.hund35.kitpvp.cmds.spawn;
import mc.hund35.kitpvp.cmds.stats;
import mc.hund35.kitpvp.events.drop;
import mc.hund35.kitpvp.events.join;
import mc.hund35.kitpvp.events.motd;
import mc.hund35.kitpvp.features.exchange;
import mc.hund35.kitpvp.guis.boots1;
import mc.hund35.kitpvp.guis.bow1;
import mc.hund35.kitpvp.guis.chestplate1;
import mc.hund35.kitpvp.guis.heks2;
import mc.hund35.kitpvp.guis.helmet1;
import mc.hund35.kitpvp.guis.leggings1;
import mc.hund35.kitpvp.guis.sword1;
import mc.hund35.kitpvp.npcs.heks;
import mc.hund35.kitpvp.npcs.upgrade;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class main
  extends JavaPlugin
  implements Listener
{
  public static main instance;
  
  public void onEnable()
  {
    instance = this;
    registerCommand();
    Bukkit.getServer().getPluginManager().registerEvents(this, this);
    Bukkit.getServer().getPluginManager().registerEvents(new motd(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new exchange(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new upgrade(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new sword1(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new bow1(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new helmet1(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new chestplate1(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new leggings1(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new boots1(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new join(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new spawn(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new drop(), this);
    spawn.onEnable();
    Bukkit.getServer().getPluginManager().registerEvents(new heks(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new heks2(), this);

    
    
    getServer().getScheduler().scheduleSyncRepeatingTask(this, new BukkitRunnable()
    {
      public void run()
      {
        for (Player p : Bukkit.getOnlinePlayers()) {
          main.this.SetScoreboard(p);
        }
      }
    }, 0L, 120L);
  }
  
  private void registerCommand()
  {
    getCommand("kit").setExecutor(new kit());
    getCommand("spawn").setExecutor(new spawn());
    getCommand("setspawn").setExecutor(new setspawn());
    getCommand("msg").setExecutor(new msg());
    getCommand("stats").setExecutor(new stats());
  }
  
  public void SetScoreboard(Player p)
  {
    Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
    Objective ob = board.registerNewObjective("sb", "dummy");
    
    File file2 = new File(instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    
    ob.setDisplaySlot(DisplaySlot.SIDEBAR);
    
    ob.setDisplayName("§3§lKit§b§lPvP");
    
    ob.getScore("§5").setScore(14);
    ob.getScore("§8§l» §b§lDin rank:").setScore(13);
    PermissionUser user = PermissionsEx.getUser(p);
    
    if(user.inGroup("EJER")){   
    	 ob.getScore("  §dEJER").setScore(12);
	}else if(user.inGroup("MANAGER")){
		 ob.getScore("  §4MANAGER").setScore(12);
	}else if(user.inGroup("ADMIN")){
		 ob.getScore("  §cADMIN").setScore(12);
	}else if(user.inGroup("MOD")){
		 ob.getScore("  §6MOD").setScore(12);
	}else if(user.inGroup("HELPER")){
		 ob.getScore("  §eHJÆLPER").setScore(12);
	} else if(user.inGroup("TITAN")){
		 ob.getScore("  §3TITAN").setScore(12);
     }else if(user.inGroup("HERO")){
    	 ob.getScore("  §bHERO").setScore(12);
     }else if(user.inGroup("LEGEND")){
    	 ob.getScore("  §aLEGEND").setScore(12);	
	  }else{
		  ob.getScore("  §fNORMAL").setScore(12);
	  }
    ob.getScore("§4").setScore(11);
    ob.getScore("§8§l» §b§lDine penge:").setScore(10);
    ob.getScore("  §f$" + config2.getInt(new StringBuilder("Config.").append(p.getName()).append(".Penge").toString())).setScore(9);
    ob.getScore("                  §0").setScore(8);
    
    ob.getScore("§8§l» §b§lDit level:").setScore(7);
    ob.getScore("  §f" + config2.getInt(new StringBuilder("Config.").append(p.getName()).append(".Level").toString())).setScore(6);
    
    ob.getScore("§d").setScore(5);
    
    ob.getScore("§8§l» §b§lKills & Deaths:").setScore(4);
    ob.getScore("  §f" + config2.getInt(new StringBuilder("Config.").append(p.getName()).append(".Kills").toString()) + " §3& §f" + config2.getInt(new StringBuilder("Config.").append(p.getName()).append(".Deaths").toString())).setScore(3);
    ob.getScore("§0").setScore(2);
    
    ob.getScore("§8§l» §b§lTeam:").setScore(1);
    ob.getScore("  §cKommer").setScore(0);
    
    p.setScoreboard(board);
  }
}
