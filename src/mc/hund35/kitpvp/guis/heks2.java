package mc.hund35.kitpvp.guis;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mc.hund35.kitpvp.main;
import mc.hund35.kitpvp.guis.items.build;

public class heks2
  implements Listener
{
  protected static Inventory inv;
  protected static Material m;
  
  public static Inventory heks(Player p)
  {
    inv = Bukkit.createInventory(null, 9, "§dHeks");
    
    build.itemType3(m.POTION, 8227, 1, "§5Ild beskyttelse", inv, 1, "§7Koster: §d$250", "§7", "§c§lBEMÆRK: §7Du mister denne buff når du dør!");
    
    build.itemType3(m.POTION, 8194, 1, "§5Speed", inv, 3, "§7Koster: §d$300", "§7", "§c§lBEMÆRK: §7Du mister denne buff når du dør!");
    
    build.itemType3(m.POTION, 8201, 1, "§5Styrke", inv, 5, "§7Koster: §d$750", "§7", "§c§lBEMÆRK: §7Du mister denne buff når du dør!");

    build.itemType3(m.POTION, 8193, 1, "§5Regeneration", inv, 7, "§7Koster: §d$500", "§7", "§c§lBEMÆRK: §7Du mister denne buff når du dør!");
    
    return inv;
  }
  
  @EventHandler
  public static void AdminP(InventoryClickEvent event)
  {

	  if (event.getInventory().getTitle().equalsIgnoreCase("§dHeks")){
		    Player p = (Player)event.getWhoClicked();
		    
			if(event.getCurrentItem() == null || event.getClickedInventory().getTitle() == null){
        event.setCancelled(true);
        return;
        

			}else{
				event.setCancelled(true);
				  if (!event.getCurrentItem().hasItemMeta()) {
		                return;
		            }
		     
				  File file = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
		    	    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
				  
	    if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Ild beskyttelse")){
	    	if(config.getInt("Config." + p.getName() + ".Penge") >= 250){
	    		config.set("Config." + p.getName() + ".Penge", config.getInt("Config." + p.getName() + ".Penge") - 250);
	    		p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 999999999, 0));
	    		p.sendMessage("§2§lHEKS §8§l» §aDu har nu købt §2ILD BESKYTTELSE§a!");
	    		try {
					config.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	}else{
	    		  p.sendMessage("§4§lHEKS §8§l» §cDu har ikke nok penge til købe denne buff!");
	    		return;
	    	}
			
        }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Speed")){
        	if(config.getInt("Config." + p.getName() + ".Penge") >= 300){
	    		config.set("Config." + p.getName() + ".Penge", config.getInt("Config." + p.getName() + ".Penge") - 300);
	    		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999999, 0));
	    		p.sendMessage("§2§lHEKS §8§l» §aDu har nu købt §2SPEED§a!");
	    		try {
					config.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	}else{
	    		  p.sendMessage("§4§lHEKS §8§l» §cDu har ikke nok penge til købe denne buff!");
	    		return;
	    	}
        	
        }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Styrke")){
        	if(config.getInt("Config." + p.getName() + ".Penge") >= 750){
	    		config.set("Config." + p.getName() + ".Penge", config.getInt("Config." + p.getName() + ".Penge") - 750);
	    		p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 999999999, 0));
	    		p.sendMessage("§2§lHEKS §8§l» §aDu har nu købt §2STYRKE§a!");
	    		try {
					config.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	}else{
	    		  p.sendMessage("§4§lHEKS §8§l» §cDu har ikke nok penge til købe denne buff!");
	    		return;
	    	}
        	
        }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Regeneration")){
        	if(config.getInt("Config." + p.getName() + ".Penge") >= 500){
	    		config.set("Config." + p.getName() + ".Penge", config.getInt("Config." + p.getName() + ".Penge") - 500);
	    		p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 999999999, 0));
	    		p.sendMessage("§2§lHEKS §8§l» §aDu har nu købt §2REGENERATION§a!");
	    		try {
					config.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	}else{
	    		  p.sendMessage("§4§lHEKS §8§l» §cDu har ikke nok penge til købe denne buff!");
	    		return;
	    	}
        	
        }
			 else{
        	event.setCancelled(true);
        }
        }
        	
        }
  
		
  }

  
  
  
  
  
  
  
  
}