package mc.hund35.kitpvp.guis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.hund35.kitpvp.main;

public class bow1
  implements Listener
{
  protected static Inventory inv;
  
  public static Inventory bow1(Player p)
  {
    inv = Bukkit.createInventory(null, 9, "§6Upgrade din §eBow");
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    if ((config2.getInt("Config." + p.getName() + ".Power") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 50))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 50))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 250))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 250))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") < 500))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") >= 500))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 3) && (config2.getInt("Config." + p.getName() + ".Penge") < 1250))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 4");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$1250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 3) && (config2.getInt("Config." + p.getName() + ".Penge") >= 1250))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 4");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$1250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 4) && (config2.getInt("Config." + p.getName() + ".Penge") < 2500))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 5");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$2500");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Power") == 4) && (config2.getInt("Config." + p.getName() + ".Penge") >= 2500))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epower 5");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$2500");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".Power") == 5)
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(1, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Flame") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 250))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eflame 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(3, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Flame") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 250))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eflame 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(3, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Flame") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 750))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eflame 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$750");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(3, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Flame") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 750))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eflame 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$750");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(3, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".Flame") == 2)
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(3, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Punch") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 50))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epunch 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(5, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Punch") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 50))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epunch 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(5, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Punch") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 125))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epunch 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$125");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(5, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".Punch") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 125))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §epunch 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$125");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(5, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".Punch") == 2)
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(5, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 50))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(7, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 50))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(7, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 250))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(7, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 250))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(7, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") < 500))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(7, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") >= 500))
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(7, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 3)
    {
      ItemStack armor = new ItemStack(Material.BOW);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(7, armor);
    }
    return inv;
  }
  
  @EventHandler
  public void onClick(InventoryClickEvent e)
  {
    if (e.getInventory().getTitle().equalsIgnoreCase("§6Upgrade din §eBow"))
    {
      e.setCancelled(true);
      Player p = (Player)e.getWhoClicked();
      File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
      YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §epower 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 50)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 50));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt power 1 til dit kit!");
        config2.set("Config." + p.getName() + ".Power", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §epower 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt power 2 til dit kit!");
        config2.set("Config." + p.getName() + ".Power", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §epower 3"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 500)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 500));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt power 3 til dit kit!");
        config2.set("Config." + p.getName() + ".Power", Integer.valueOf(3));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §epower 4"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 1250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 1250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt power 4 til dit kit!");
        config2.set("Config." + p.getName() + ".Power", Integer.valueOf(4));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §epower 5"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 2500)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 2500));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt power 5 til dit kit!");
        config2.set("Config." + p.getName() + ".Power", Integer.valueOf(5));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eflame 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt flame 1 til dit kit!");
        config2.set("Config." + p.getName() + ".Flame", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eflame 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 750)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 750));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt flame 2 til dit kit!");
        config2.set("Config." + p.getName() + ".Flame", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §epunch 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 50)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 50));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt punch 1 til dit kit!");
        config2.set("Config." + p.getName() + ".Punch", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §epunch 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 125)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 125));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt punch 2 til dit kit!");
        config2.set("Config." + p.getName() + ".Punch", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eunbreaking 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 50)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 50));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt unbreaking 1 til dit kit!");
        config2.set("Config." + p.getName() + ".BowUnbreaking", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eunbreaking 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt unbreaking 2 til dit kit!");
        config2.set("Config." + p.getName() + ".BowUnbreaking", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eunbreaking 3"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 500)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 500));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt unbreaking 3 til dit kit!");
        config2.set("Config." + p.getName() + ".BowUnbreaking", Integer.valueOf(3));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Maximum level")) {
        e.setCancelled(true);
      }
    }
  }
}
