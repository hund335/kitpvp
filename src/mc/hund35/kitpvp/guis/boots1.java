package mc.hund35.kitpvp.guis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.hund35.kitpvp.main;

public class boots1
  implements Listener
{
  protected static Inventory inv;
  
  public static Inventory boots1(Player p)
  {
    inv = Bukkit.createInventory(null, 9, "§6Upgrade din §eBoots");
    File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
    if ((config2.getInt("Config." + p.getName() + ".BProtection") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprotection 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProtection") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprotection 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if ((config2.getInt("Config." + p.getName() + "..BProtection") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprotection 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProtection") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprotection 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProtection") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") < 750))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprotection 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$750");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProtection") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") >= 750))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprotection 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$750");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProtection") == 3) && (config2.getInt("Config." + p.getName() + ".Penge") < 2000))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprotection 4");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$2000");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProtection") == 3) && (config2.getInt("Config." + p.getName() + ".Penge") >= 2000))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprotection 4");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$2000");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".BProtection") == 4)
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(0, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BFireProt") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §efire protection 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BFireProt") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §efire protection 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BFireProt") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §efire protection 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BFireProt") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §efire protection 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BFireProt") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") < 500))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §efire protection 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BFireProt") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") >= 500))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §efire protection 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BFireProt") == 3) && (config2.getInt("Config." + p.getName() + ".Penge") < 1250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §efire protection 4");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$1250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BFireProt") == 3) && (config2.getInt("Config." + p.getName() + ".Penge") >= 1250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §efire protection 4");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$1250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".BFireProt") == 4)
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(2, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProjProt") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprojectile protection 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProjProt") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprojectile protection 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProjProt") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprojectile protection 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProjProt") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprojectile protection 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProjProt") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") < 500))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprojectile protection 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProjProt") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") >= 500))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprojectile protection 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProjProt") == 3) && (config2.getInt("Config." + p.getName() + ".Penge") < 1250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprojectile protection 4");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$1250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BProjProt") == 3) && (config2.getInt("Config." + p.getName() + ".Penge") >= 1250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eprojectile protection 4");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$1250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".BProjProt") == 4)
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(4, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BUnbreaking") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(6, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BUnbreaking") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$50");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(6, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BUnbreaking") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(6, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BUnbreaking") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(6, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BUnbreaking") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") < 500))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(6, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BUnbreaking") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") >= 500))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §eunbreaking 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$500");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(6, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".BUnbreaking") == 3)
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(6, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BThorns") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") < 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §ethorns 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$125");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(8, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BThorns") == 0) && (config2.getInt("Config." + p.getName() + ".Penge") >= 50))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §ethorns 1");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$125");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(8, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BThorns") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") < 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §ethorns 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e250");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(8, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BThorns") == 1) && (config2.getInt("Config." + p.getName() + ".Penge") >= 250))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §ethorns 2");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$250");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(8, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BThorns") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") < 500))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §ethorns 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$750");
      lore.add("§c§oDu har ikke nok penge");
      lore.add("§c§otil at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(8, armor);
    }
    if ((config2.getInt("Config." + p.getName() + ".BThorns") == 2) && (config2.getInt("Config." + p.getName() + ".Penge") >= 500))
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§6Upgrade til §ethorns 3");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§6Pris: §e$750");
      lore.add("§a§oTryk her for at købe denne upgrade!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(8, armor);
    }
    if (config2.getInt("Config." + p.getName() + ".BThorns") == 3)
    {
      ItemStack armor = new ItemStack(Material.DIAMOND_BOOTS);
      ItemMeta meta = armor.getItemMeta();
      meta.setDisplayName("§4Maximum level");
      armor.getDurability();
      List<String> lore = new ArrayList();
      lore.add("§c§oDenne enchantment kan ikke blive upgraded mere!");
      meta.setLore(lore);
      armor.setItemMeta(meta);
      inv.setItem(8, armor);
    }
    return inv;
  }
  
  @EventHandler
  public void onClick(InventoryClickEvent e)
  {
    if (e.getInventory().getTitle().equalsIgnoreCase("§6Upgrade din §eBoots"))
    {
      e.setCancelled(true);
      Player p = (Player)e.getWhoClicked();
      File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
      YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eprotection 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 50)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 50));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt protection 1 til dit kit!");
        config2.set("Config." + p.getName() + ".BProtection", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eprotection 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt protection 2 til dit kit!");
        config2.set("Config." + p.getName() + ".BProtection", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eprotection 3"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 750)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 750));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt protection 3 til dit kit!");
        config2.set("Config." + p.getName() + ".BProtection", Integer.valueOf(3));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eprotection 4"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 2000)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 2000));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt protection 4 til dit kit!");
        config2.set("Config." + p.getName() + ".BProtection", Integer.valueOf(4));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §efire protection 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 50)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 50));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt fire protection 1 til dit kit!");
        config2.set("Config." + p.getName() + ".BFireProt", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §efire protection 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt fire protection 2 til dit kit!");
        config2.set("Config." + p.getName() + ".BFireProt", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §efire protection 3"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 500)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 500));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt fire protection 3 til dit kit!");
        config2.set("Config." + p.getName() + ".BFireProt", Integer.valueOf(3));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §efire protection 4"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 1250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 1250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt fire protection 4 til dit kit!");
        config2.set("Config." + p.getName() + ".BFireProt", Integer.valueOf(4));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eprojectile protection 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 50)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 50));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt projectile protection 1 til dit kit!");
        config2.set("Config." + p.getName() + ".BProjProt", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eprojectile protection 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt projectile protection 2 til dit kit!");
        config2.set("Config." + p.getName() + ".BProjProt", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eprojectile protection 3"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 500)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 500));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt projectile protection 3 til dit kit!");
        config2.set("Config." + p.getName() + ".BProjProt", Integer.valueOf(3));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eprojectile protection 4"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 1250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 1250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt projectile protection 4 til dit kit!");
        config2.set("Config." + p.getName() + ".BProjProt", Integer.valueOf(4));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eunbreaking 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 50)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 50));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt unbreaking 1 til dit kit!");
        config2.set("Config." + p.getName() + ".BUnbreaking", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eunbreaking 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt unbreaking 2 til dit kit!");
        config2.set("Config." + p.getName() + ".BUnbreaking", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §eunbreaking 3"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 500)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 500));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt unbreaking 3 til dit kit!");
        config2.set("Config." + p.getName() + ".BUnbreaking", Integer.valueOf(3));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §ethorns 1"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 125)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 125));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt thorns 1 til dit kit!");
        config2.set("Config." + p.getName() + ".BThorns", Integer.valueOf(1));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §ethorns 2"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 250)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 250));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt thorns 2 til dit kit!");
        config2.set("Config." + p.getName() + ".BThorns", Integer.valueOf(2));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Upgrade til §ethorns 3"))
      {
        if (config2.getInt("Config." + p.getName() + ".Penge") < 750)
        {
          p.sendMessage("§4§lUPGRADE §8§l» §cDu har ikke nok penge til kunne upgrade denne enchant!");
          e.setCancelled(true);
          return;
        }
        config2.set("Config." + p.getName() + ".Penge", Integer.valueOf(config2.getInt("Config." + p.getName() + ".Penge") - 750));
        p.sendMessage("§2§lUPGRADE §8§l» §aTillykke, du har nu købt thorns 3 til dit kit!");
        config2.set("Config." + p.getName() + ".BThorns", Integer.valueOf(3));
        p.closeInventory();
        e.setCancelled(true);
        p.closeInventory();
        try
        {
          config2.save(file2);
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Maximum level")) {
        e.setCancelled(true);
      }
    }
  }
}
