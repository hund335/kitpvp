package mc.hund35.kitpvp.cmds;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import mc.hund35.kitpvp.main;

public class spawn
  implements CommandExecutor, Listener
{
  public static void onEnable()
  {
    Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(main.instance, new Runnable()
    {
      public void run()
      {
        for (Player p : Bukkit.getOnlinePlayers())
        {
          File file = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
          YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
          if (config.getBoolean("Config." + p.getName() + ".Teleport"))
          {
            config.set("Config." + p.getName() + ".Countdown", Integer.valueOf(config.getInt("Config." + p.getName() + ".Countdown") - 1));
            try
            {
              config.save(file);
            }
            catch (IOException e)
            {
              e.printStackTrace();
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 10) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 9) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 8) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 7) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 6) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 5) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 4) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 3) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 2) {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekunder§7!");
            }
            if (config.getInt("Config." + p.getName() + ".Countdown") == 1)
            {
              p.sendMessage("§b§lSPAWN §8§l» §7Du bliver teleporteret om §a" + config.getInt(new StringBuilder("Config.").append(p.getName()).append(".Countdown").toString()) + " sekund§7!");
              config.set("Config." + p.getName() + ".Countdown", Integer.valueOf(0));
              config.set("Config." + p.getName() + ".Teleport", Boolean.valueOf(false));
              try
              {
                config.save(file);
              }
              catch (IOException e)
              {
                e.printStackTrace();
              }
              File file2 = new File(main.instance.getDataFolder().getPath(), "data/spawn.yml");
              YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
              World world = Bukkit.getServer().getWorld(config2.getString("Config.Spawn.World"));
              p.teleport(new Location(world, config2.getInt("Config.Spawn.X"), config2.getInt("Config.Spawn.Y"), config2.getInt("Config.Spawn.Z"), config2.getInt("Config.Spawn.Yaw"), config2.getInt("Config.Spawn.Pitch")));
            }
          }
        }
      }
    }, 0L, 20L);
  }
  
  @EventHandler
  public void Move(PlayerMoveEvent e)
  {
    Player p = e.getPlayer();
    File file = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    if (config.getBoolean("Config." + p.getName() + ".Teleport"))
    {
      p.sendMessage("§4§lSPAWN §8§l» §cDesværre du bevæget dig, så derfor er din spawn request blevet annulleret!");
      
      config.set("Config." + p.getName() + ".Teleport", Boolean.valueOf(false));
      config.set("Config." + p.getName() + ".Countdown", Integer.valueOf(0));
      try
      {
        config.save(file);
      }
      catch (IOException e2)
      {
        e2.printStackTrace();
      }
    }
  }
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§4§lSPAWN §8§l» §cDenne kommando kan kun bruges in-game!");
      return true;
    }
    Player p = (Player)sender;
    if ((commandLabel.equalsIgnoreCase("spawn")) && 
      (p.hasPermission("spawn.use")) && 
      (args.length == 0))
    {
      File file = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
      YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
      
      config.set("Config." + p.getName() + ".Countdown", Integer.valueOf(10));
      config.set("Config." + p.getName() + ".Teleport", Boolean.valueOf(true));
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
    }
    return false;
  }
}
