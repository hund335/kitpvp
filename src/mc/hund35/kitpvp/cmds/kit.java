package mc.hund35.kitpvp.cmds;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import mc.hund35.kitpvp.main;

public class kit
  implements CommandExecutor
{
  public Map<String, String> oto = new HashMap();
  public HashMap<String, Long> cooldowns = new HashMap();
  
  public void onDisable() {}
  
  public void onEnable() {}
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§4§lKIT §8§l» §cDenne kommando kan kun bruges in-game!");
      return true;
    }
    Player p = (Player)sender;
    if ((commandLabel.equalsIgnoreCase("Kit")) && 
      (p.hasPermission("kit.use")) && 
      (args.length == 0))
    {
      int cooldownTime1 = 30;
      if (this.cooldowns.containsKey(sender.getName()))
      {
        long secondsLeft = ((Long)this.cooldowns.get(sender.getName())).longValue() / 1000L + cooldownTime1 - System.currentTimeMillis() / 1000L;
        if (secondsLeft > 0L)
        {
          p.sendMessage("§4§lKIT §8§l» §cDu kan først tage dit kit igen om " + secondsLeft + " sekunder!");
          return true;
        }
      }
      this.cooldowns.put(sender.getName(), Long.valueOf(System.currentTimeMillis()));
      File file2 = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
      YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
      
      PlayerInventory pi = p.getInventory();
      
      ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
      if (config2.getInt("Config." + p.getName() + ".Sharpness") == 1) {
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".Sharpness") == 2) {
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".Sharpness") == 3) {
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".Sharpness") == 4) {
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".Sharpness") == 5) {
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 5);
      }
      if (config2.getInt("Config." + p.getName() + ".Fire") == 1) {
        sword.addEnchantment(Enchantment.FIRE_ASPECT, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".Fire") == 2) {
        sword.addEnchantment(Enchantment.FIRE_ASPECT, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".Knockback") == 1) {
        sword.addEnchantment(Enchantment.KNOCKBACK, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".Knockback") == 2) {
        sword.addEnchantment(Enchantment.KNOCKBACK, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".SUnbreaking") == 1) {
        sword.addEnchantment(Enchantment.DURABILITY, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".SUnbreaking") == 2) {
        sword.addEnchantment(Enchantment.DURABILITY, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".SUnbreaking") == 3) {
        sword.addEnchantment(Enchantment.DURABILITY, 3);
      }
      pi.addItem(new ItemStack[] { sword });
      
      ItemStack bow = new ItemStack(Material.BOW, 1);
      if (config2.getInt("Config." + p.getName() + ".Power") == 1) {
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".Power") == 2) {
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".Power") == 3) {
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".Power") == 4) {
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".Power") == 5) {
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
      }
      if (config2.getInt("Config." + p.getName() + ".Flame") == 1) {
        bow.addEnchantment(Enchantment.ARROW_FIRE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".Flame") == 2) {
        bow.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".Punch") == 1) {
        bow.addEnchantment(Enchantment.ARROW_KNOCKBACK, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".Punch") == 2) {
        bow.addEnchantment(Enchantment.ARROW_KNOCKBACK, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 1) {
        bow.addEnchantment(Enchantment.DURABILITY, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 2) {
        bow.addEnchantment(Enchantment.DURABILITY, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".BowUnbreaking") == 3) {
        bow.addEnchantment(Enchantment.DURABILITY, 3);
      }
      pi.addItem(new ItemStack[] { bow });
      
      ItemStack helmet = new ItemStack(Material.DIAMOND_HELMET, 1);
      if (config2.getInt("Config." + p.getName() + ".HProtection") == 1) {
        helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".HProtection") == 2) {
        helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".HProtection") == 3) {
        helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".HProtection") == 4) {
        helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".HFireProt") == 1) {
        helmet.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".HFireProt") == 2) {
        helmet.addEnchantment(Enchantment.PROTECTION_FIRE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".HFireProt") == 3) {
        helmet.addEnchantment(Enchantment.PROTECTION_FIRE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".HFireProt") == 4) {
        helmet.addEnchantment(Enchantment.PROTECTION_FIRE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".HProjProt") == 1) {
        helmet.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".HProjProt") == 2) {
        helmet.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".HProjProt") == 3) {
        helmet.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".HProjProt") == 4) {
        helmet.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".HUnbreaking") == 1) {
        helmet.addEnchantment(Enchantment.DURABILITY, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".HUnbreaking") == 2) {
        helmet.addEnchantment(Enchantment.DURABILITY, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".HUnbreaking") == 3) {
        helmet.addEnchantment(Enchantment.DURABILITY, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".HThorns") == 1) {
        helmet.addEnchantment(Enchantment.THORNS, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".HThorns") == 2) {
        helmet.addEnchantment(Enchantment.THORNS, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".HThorns") == 3) {
        helmet.addEnchantment(Enchantment.THORNS, 3);
      }
      pi.addItem(new ItemStack[] { helmet });
      
      ItemStack chestp = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
      if (config2.getInt("Config." + p.getName() + ".CProtection") == 1) {
        chestp.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".CProtection") == 2) {
        chestp.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".CProtection") == 3) {
        chestp.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".CProtection") == 4) {
        chestp.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".CFireProt") == 1) {
        chestp.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".CFireProt") == 2) {
        chestp.addEnchantment(Enchantment.PROTECTION_FIRE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".CFireProt") == 3) {
        chestp.addEnchantment(Enchantment.PROTECTION_FIRE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".CFireProt") == 4) {
        chestp.addEnchantment(Enchantment.PROTECTION_FIRE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".CProjProt") == 1) {
        chestp.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".CProjProt") == 2) {
        chestp.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".CProjProt") == 3) {
        chestp.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".CProjProt") == 4) {
        chestp.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".CUnbreaking") == 1) {
        chestp.addEnchantment(Enchantment.DURABILITY, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".CUnbreaking") == 2) {
        chestp.addEnchantment(Enchantment.DURABILITY, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".CUnbreaking") == 3) {
        chestp.addEnchantment(Enchantment.DURABILITY, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".CThorns") == 1) {
        chestp.addEnchantment(Enchantment.THORNS, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".CThorns") == 2) {
        chestp.addEnchantment(Enchantment.THORNS, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".CThorns") == 3) {
        chestp.addEnchantment(Enchantment.THORNS, 3);
      }
      pi.addItem(new ItemStack[] { chestp });
      
      ItemStack leggings = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
      if (config2.getInt("Config." + p.getName() + ".LProtection") == 1) {
        leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".LProtection") == 2) {
        leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".LProtection") == 3) {
        leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".LProtection") == 4) {
        leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".LFireProt") == 1) {
        leggings.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".LFireProt") == 2) {
        leggings.addEnchantment(Enchantment.PROTECTION_FIRE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".LFireProt") == 3) {
        leggings.addEnchantment(Enchantment.PROTECTION_FIRE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".LFireProt") == 4) {
        leggings.addEnchantment(Enchantment.PROTECTION_FIRE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".LProjProt") == 1) {
        leggings.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".LProjProt") == 2) {
        leggings.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".LProjProt") == 3) {
        leggings.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".LProjProt") == 4) {
        leggings.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".LUnbreaking") == 1) {
        leggings.addEnchantment(Enchantment.DURABILITY, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".LUnbreaking") == 2) {
        leggings.addEnchantment(Enchantment.DURABILITY, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".LUnbreaking") == 3) {
        leggings.addEnchantment(Enchantment.DURABILITY, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".LThorns") == 1) {
        leggings.addEnchantment(Enchantment.THORNS, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".LThorns") == 2) {
        leggings.addEnchantment(Enchantment.THORNS, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".LThorns") == 3) {
        leggings.addEnchantment(Enchantment.THORNS, 3);
      }
      pi.addItem(new ItemStack[] { leggings });
      
      ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS, 1);
      if (config2.getInt("Config." + p.getName() + ".BProtection") == 1) {
        boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".BProtection") == 2) {
        boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".BProtection") == 3) {
        boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".BProtection") == 4) {
        boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".BFireProt") == 1) {
        boots.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".BFireProt") == 2) {
        boots.addEnchantment(Enchantment.PROTECTION_FIRE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".BFireProt") == 3) {
        boots.addEnchantment(Enchantment.PROTECTION_FIRE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".BFireProt") == 4) {
        boots.addEnchantment(Enchantment.PROTECTION_FIRE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".BProjProt") == 1) {
        boots.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".BProjProt") == 2) {
        boots.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".BProjProt") == 3) {
        boots.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".BProjProt") == 4) {
        boots.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4);
      }
      if (config2.getInt("Config." + p.getName() + ".BUnbreaking") == 1) {
        boots.addEnchantment(Enchantment.DURABILITY, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".BUnbreaking") == 2) {
        boots.addEnchantment(Enchantment.DURABILITY, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".BUnbreaking") == 3) {
        boots.addEnchantment(Enchantment.DURABILITY, 3);
      }
      if (config2.getInt("Config." + p.getName() + ".BThorns") == 1) {
        boots.addEnchantment(Enchantment.THORNS, 1);
      }
      if (config2.getInt("Config." + p.getName() + ".BThorns") == 2) {
        boots.addEnchantment(Enchantment.THORNS, 2);
      }
      if (config2.getInt("Config." + p.getName() + ".BThorns") == 3) {
        boots.addEnchantment(Enchantment.THORNS, 3);
      }
      pi.addItem(new ItemStack[] { boots });
      
      ItemStack arrows = new ItemStack(Material.ARROW, 32);
      pi.addItem(new ItemStack[] { arrows });
      
      p.updateInventory();
    }
    return false;
  }
}
