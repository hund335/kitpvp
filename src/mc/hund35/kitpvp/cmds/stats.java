package mc.hund35.kitpvp.cmds;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import mc.hund35.kitpvp.main;

public class stats
  implements CommandExecutor
{
  public Map<String, String> oto = new HashMap();
  
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
  {
    Player p = (Player)sender;
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§4§lSTATS §8§l» §cDenne kommando kan kun bruges in-game!");
      return true;
    }
    sender.getName().equalsIgnoreCase("Stats");
    if (args.length == 0)
    {
      Player player = (Player)sender;
      File file = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
      YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
      sender.sendMessage("§b§m§l-----------------------");
      sender.sendMessage(" §6Dine stats:");
      sender.sendMessage("");
      sender.sendMessage("    §6Penge: §e$" + config.getInt(new StringBuilder("Config.").append(player.getName()).append(".Penge").toString()));
      sender.sendMessage("    §6Level: §e" + config.getInt(new StringBuilder("Config.").append(player.getName()).append(".Level").toString()));
      sender.sendMessage("    §6Kills: §e" + config.getInt(new StringBuilder("Config.").append(player.getName()).append(".Kills").toString()));
      sender.sendMessage("    §6Deaths: §e" + config.getInt(new StringBuilder("Config.").append(player.getName()).append(".Deaths").toString()));
      if ((config.getInt("Config." + p.getName() + ".Kills") > 0) && (config.getInt("Config." + p.getName() + ".Deaths") > 0))
      {
        double kdr = config.getInt("Config." + p.getName() + ".Kills") / config.getInt("Config." + p.getName() + ".Deaths");
        sender.sendMessage("    §6KDR: §e" + kdr);
        sender.sendMessage("    §6Killstreak: §e" + config.getInt(new StringBuilder("Config.").append(player.getName()).append(".Killstreak").toString()));
        sender.sendMessage("    §6Vote points: §e" + config.getInt(new StringBuilder("Config.").append(player.getName()).append(".VP").toString()));
        
        sender.sendMessage("§b§m§l-----------------------");
      }
      else
      {
        sender.sendMessage("    §6KDR: §e0.0");
        sender.sendMessage("    §6Killstreak: §e" + config.getInt(new StringBuilder("Config.").append(player.getName()).append(".Killstreak").toString()));
        sender.sendMessage("    §6Vote points: §e" + config.getInt(new StringBuilder("Config.").append(player.getName()).append(".VP").toString()));
        
        sender.sendMessage("§b§m§l-----------------------");
      }
      return false;
    }
    if (args.length >= 1)
    {
      String targetName = args[0];
      Player player = (Player)sender;
      Player target = Bukkit.getPlayer(targetName);
      if (target == null)
      {
        player.sendMessage("§4§lSTATS §8§l» §cSpilleren blev ikke fundet!");
        return true;
      }
      File file = new File(main.instance.getDataFolder().getPath(), "players/" + target.getName() + ".yml");
      YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
      
      sender.sendMessage("§b§m§l-----------------------");
      sender.sendMessage(" §6" + target.getName() + "'s Stats");
      sender.sendMessage("");
      sender.sendMessage("    §6Penge: §e$" + config.getInt(new StringBuilder("Config.").append(target.getName()).append(".Penge").toString()));
      sender.sendMessage("    §6Level: §e" + config.getInt(new StringBuilder("Config.").append(target.getName()).append(".Level").toString()));
      sender.sendMessage("    §6Kills: §e" + config.getInt(new StringBuilder("Config.").append(target.getName()).append(".Kills").toString()));
      sender.sendMessage("    §6Deaths: §e" + config.getInt(new StringBuilder("Config.").append(target.getName()).append(".Deaths").toString()));
      if ((config.getInt("Config." + target.getName() + ".Kills") > 0) && (config.getInt("Config." + target.getName() + ".Deaths") > 0))
      {
        double kdr = config.getInt("Config." + p.getName() + ".Kills") / config.getInt("Config." + p.getName() + ".Deaths");
        sender.sendMessage("    §6KDR: §e" + kdr);
        sender.sendMessage("    §6Killstreak: §e" + config.getInt(new StringBuilder("Config.").append(target.getName()).append(".Killstreak").toString()));
        sender.sendMessage("    §6Vote points: §e" + config.getInt(new StringBuilder("Config.").append(target.getName()).append(".VP").toString()));
        
        sender.sendMessage("§b§m§l-----------------------");
      }
      else
      {
        sender.sendMessage("    §6KDR: §e0.0");
        sender.sendMessage("    §6Killstreak: §e" + config.getInt(new StringBuilder("Config.").append(target.getName()).append(".Killstreak").toString()));
        sender.sendMessage("    §6Vote points: §e" + config.getInt(new StringBuilder("Config.").append(target.getName()).append(".VP").toString()));
        
        sender.sendMessage("§b§m§l-----------------------");
      }
    }
    return false;
  }
}
