package mc.hund35.kitpvp.cmds;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class msg
  implements CommandExecutor
{
  public Map<String, String> oto = new HashMap();
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§4§lMSG §8§l» §cDenne kommando kan kun bruges in-game!");
      return true;
    }
    Player p = (Player)sender;
    if ((commandLabel.equalsIgnoreCase("msg")) && 
      (p.hasPermission("msg.use")))
    {
      if (args.length < 2)
      {
        sender.sendMessage("§b§lMSG §8§l» §cBrug: /msg <spiller> <besked>");
        return true;
      }
      if (args.length >= 1)
      {
        String targetName = args[0];
        Player player = (Player)sender;
        Player target = Bukkit.getPlayer(targetName);
        if (target == null)
        {
          player.sendMessage("§4§lMSG §8§l» §cSpilleren blev ikke fundet!");
          
          return true;
        }
        if (args.length >= 2)
        {
          String msg = "";
          for (int i = 1; i < args.length; i++) {
            msg = msg + args[i] + " ";
          }
          sender.sendMessage("§8[§3mig §b» §3" + target.getName() + "§8] §7" + msg);
          target.sendMessage("§8[§3" + sender.getName() + " §b» §3mig" + "§8] §7" + msg);
        }
      }
    }
    return false;
  }
}
