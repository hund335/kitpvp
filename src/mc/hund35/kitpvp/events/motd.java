package mc.hund35.kitpvp.events;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import mc.hund35.kitpvp.main;

public class motd
  implements Listener
{
  @EventHandler
  public void Motd(PlayerJoinEvent e)
  {
    Player p = e.getPlayer();
    p.sendMessage("§8§l§m#------------------------------------#");
    p.sendMessage("§6/hj§lp §8- §efor at se alle vores kommandoer!");
    p.sendMessage("§6/info §8- §efor at se alt infomation om serveren!");
    p.sendMessage("§6/regler §8- §efor at se alle vores regler!");
    p.sendMessage("§6/buy §8- §efor at hj§lpe serveren!");
    p.sendMessage("§6/warns §8- §efor at se dine advarseler!");
    p.sendMessage("§8§l§m#------------------------------------#");
    if (!p.hasPlayedBefore())
    {
      Bukkit.savePlayers();
      Bukkit.getServer().savePlayers();
      File file = new File(main.instance.getDataFolder().getPath(), "data/server.yml");
      YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
      config.set("Config.Total", Integer.valueOf(config.getInt("Config.Total") + 1));
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
      File file2 = new File(main.instance.getDataFolder().getPath(), "data/spawn.yml");
      YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
      World world = Bukkit.getServer().getWorld(config2.getString("Config.Spawn.World"));
      p.teleport(new Location(world, config2.getInt("Config.Spawn.X"), config2.getInt("Config.Spawn.Y"), config2.getInt("Config.Spawn.Z"), config2.getInt("Config.Spawn.Yaw"), config2.getInt("Config.Spawn.Pitch")));
      
      Bukkit.broadcastMessage("§e§lNY §8§l» §7Velkommen til §a" + p.getName() + "§7, han/hun er nummer #§2" + config.getInt("Config.Total") + "§7!");
    }
  }
}
