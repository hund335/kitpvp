package mc.hund35.kitpvp.events;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import mc.hund35.kitpvp.main;

public class join
  implements Listener
{
  @EventHandler
  public void onJoin(PlayerJoinEvent event)
  {
    Player p = event.getPlayer();
    File file = new File(main.instance.getDataFolder().getPath(), "players/" + p.getName() + ".yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    config.set("Config." + p.getName() + ".IP", "" + p.getAddress());
    try
    {
      config.save(file);
    }
    catch (IOException e2)
    {
      e2.printStackTrace();
    }
 
    if (config.getString("Config." + p.getName() + ".Level") == null)
    {
      config.set("Config." + p.getName() + ".Level", Integer.valueOf(1));
      try
      {
        config.save(file);
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
    }
    event.setJoinMessage("");
  }
  
  @EventHandler
  public void onQuit(PlayerQuitEvent event)
  {
    Player player = event.getPlayer();
    
    event.setQuitMessage("");
  }
}
