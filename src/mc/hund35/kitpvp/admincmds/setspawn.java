package mc.hund35.kitpvp.admincmds;

import java.io.File;
import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import mc.hund35.kitpvp.main;

public class setspawn
  implements CommandExecutor, Listener
{
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§4§lSETSPAWN §8§l§ §cDenne kommando kan kun bruges in-game!");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("setspawn")) {
      if (p.hasPermission("setspawn.use"))
      {
        if (args.length == 0)
        {
          File file = new File(main.instance.getDataFolder().getPath(), "data/spawn.yml");
          YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
          config.set("Config.Spawn.World", p.getLocation().getWorld().getName());
          config.set("Config.Spawn.X", Integer.valueOf(p.getLocation().getBlockX()));
          config.set("Config.Spawn.Y", Integer.valueOf(p.getLocation().getBlockY()));
          config.set("Config.Spawn.Z", Integer.valueOf(p.getLocation().getBlockZ()));
          config.set("Config.Spawn.Pitch", Float.valueOf(p.getLocation().getPitch()));
          config.set("Config.Spawn.Yaw", Float.valueOf(p.getLocation().getYaw()));
          p.sendMessage("§b§lSETSPAWN §8§l» §7Du har nu sat spawn locationen.");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
      }
      else {
        p.sendMessage("§4§lSETSPAWN §8§l» §cDu har ikke tillades til denne kommando!");
      }
    }
    return false;
  }
}
