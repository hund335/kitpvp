package mc.hund35.kitpvp.npcs;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import mc.hund35.kitpvp.guis.boots1;
import mc.hund35.kitpvp.guis.bow1;
import mc.hund35.kitpvp.guis.chestplate1;
import mc.hund35.kitpvp.guis.helmet1;
import mc.hund35.kitpvp.guis.leggings1;
import mc.hund35.kitpvp.guis.sword1;
import net.citizensnpcs.api.event.NPCRightClickEvent;

public class upgrade
  implements Listener
{
  @EventHandler(priority=EventPriority.NORMAL, ignoreCancelled=true)
  public void npcRightClickEvent(NPCRightClickEvent e)
  {
    if (e.getNPC().getId() == 16)
    {
      if (e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_SWORD))
      {
        e.getClicker().openInventory(sword1.sword1(e.getClicker()));
        return;
      }
      if (e.getClicker().getItemInHand().getType().equals(Material.BOW))
      {
        e.getClicker().openInventory(bow1.bow1(e.getClicker()));
        return;
      }
      if (e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_HELMET))
      {
        e.getClicker().openInventory(helmet1.helmet1(e.getClicker()));
        return;
      }
      if (e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_CHESTPLATE))
      {
        e.getClicker().openInventory(chestplate1.chestplate1(e.getClicker()));
        return;
      }
      if (e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_LEGGINGS))
      {
        e.getClicker().openInventory(leggings1.leggings1(e.getClicker()));
        return;
      }
      if (e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_BOOTS))
      {
        e.getClicker().openInventory(boots1.boots1(e.getClicker()));
        return;
      }
      if ((!e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_SWORD)) || (!e.getClicker().getItemInHand().getType().equals(Material.BOW)) || (!e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_HELMET)) || (!e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_CHESTPLATE)) || (!e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_LEGGINGS)) || (!e.getClicker().getItemInHand().getType().equals(Material.DIAMOND_BOOTS)))
      {
        e.getClicker().sendMessage("§4§lUPGRADE §8§l» §cDenne ting kan ikke blive upgraded!");
        return;
      }
    }
  }
}
